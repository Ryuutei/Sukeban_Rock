<!-- -*- coding: utf-8 -*- -->
# Sukeban Rock, English subtitles

- Original Title:: 女番長ロック，闇に散る花弁（On'na banchō rokku, yami ni chiru hanabira）
- Date:: circa 1998
- Original transcript & translation by:: Ｔｏｍｏ
- Review & adaptation:: Sebastien Blanc 龍帝（2014-04-01）
- Subtitile file:: 2015-08-16
- Licence:: Creative Commons BY


## Files in this repository

* `SukebanRoku.ass`（unicode subtitles）
* `SukebanRoku.srt`（ascii subtitles—for softwares that don't support unicode like VLC—）
* `sukeban_roku_english.html`（translation notes）
* `sukeban_roku_english.txt`（translation notes source）
* `LICENSE`（the licence file）


Use the any of the subtitles file with your DVD or DVD rip of the movie.

